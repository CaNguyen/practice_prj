<?php echo('<?xml version="1.0" encoding="UTF-8"?>'); ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--responsive or smartphone-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<!--responsive or smartphone-->
<?php include(APP_PATH.'libs/argument.php');  ?>
<title><?php echo $titlepage?></title>
<meta name="description" content="<?php echo $desPage; ?>">
<meta name="keywords" content="<?php echo $keyPage; ?>" />

<!--facebook-->
<meta property="og:title" content="<?php echo $titlepage?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<? echo 'http://';echo $_SERVER["SERVER_NAME"];echo $_SERVER["SCRIPT_NAME"];echo $_SERVER["QUERY_STRING"];?>">
<meta property="og:image" content="<?php echo APP_URL;?>common/img/other/fb_image.jpg">
<meta property="og:site_name" content="">
<meta property="og:description" content="<?php echo $desPage; ?>" />
<meta property="fb:app_id" content="">
<!--/facebook-->

<!--css-->
<link rel="stylesheet" href="<?php echo APP_URL;?>common/css/base.css">
<link rel="stylesheet" href="<?php echo APP_URL;?>common/css/style.css">
<!--/css-->

<!-- Favicons ==================================================-->
<link rel="icon" href="<?php echo APP_URL;?>common/img/icon/favicon.ico" type="image/vnd.microsoft.icon" />

<!--[if lt IE 9]>
<script src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?php
	//wp_head();
?>
